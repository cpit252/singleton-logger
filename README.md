# Singleton Logger
A Logger object that needs to be implemented as a singleton class. It's used to log messages from any application component.

## Build instructions
From the command line:

- Clone the repo

```
git clone https://gitlab.com/CPIT252/singleton-logger.git
cd singleton-logger/
```

- Compile and run

```
javac -d ./out ./src/ordertracker/*.java
cd out
java ordertracker.App
```


## Importing the project into your IDE such as IntelliJ IDEA, Eclipse, or Apache Netbeans.

1. Clone or download the project archive file (e.g., zip file).
2. Unpack and import the project.
3. Mark the `src` directory as the root source directory.
4. Mark the directory named `ordertracker` as a package.
5. Change the java compiler version to be 1.8 or higher since we're using the java.time.LocalDate, which is available since 1.8.
6. Create Run configuration and select the runtime (JRE) to be 1.8 or higher.

